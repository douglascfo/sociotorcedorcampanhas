package br.com.campanhasociotorcedor.api;

import java.text.SimpleDateFormat;
import java.util.Date;

import lombok.AllArgsConstructor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.campanhasociotorcedor.api.util.CampanhaUtil;
import br.com.campanhasociotorcedor.domain.orm.Campanha;
import br.com.campanhasociotorcedor.service.CampanhaService;

//API de serviço da campanha,usada para CRUD e outros serviços da campanha 
@RestController
@AllArgsConstructor
@RequestMapping("campanha")
public class CampanhaRestService {
	
	@Autowired
    private CampanhaService campanhaService;
	private	SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy");
	private	String dataAtual = sdf.format(new Date());
	
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Campanha getById(@PathVariable Integer id) {
    	return campanhaService.getById(id);
    }
        
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/listaCampanhasAtivas", method = RequestMethod.GET)
    public Iterable<Campanha> getAllCampanhasAtivas() {
    	return campanhaService.findAllCurrent(dataAtual);
    }
        
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/carregamentoPadrao", method = RequestMethod.GET)
    public String carregaCampanhas() {  
    	String dataInicio = "06/12/2017";
    	String dataTermino = "30/12/2018";
    	
    	Campanha campanha = new Campanha();
    	campanha.setIdTimeCoracao(1);
    	campanha.setNomeCampanha("Campanha do Corinthians");
    	campanha.setDataVigenciaInicial(dataInicio);
    	campanha.setDataVigenciaFinal(dataTermino);
    	campanhaService.saveOrUpdate(campanha);    	
    	Campanha campanha1 = new Campanha();
    	campanha1.setIdTimeCoracao(2);
    	campanha1.setNomeCampanha("Campanha do São Paulo");
    	campanha1.setDataVigenciaInicial(dataInicio); 
    	campanha1.setDataVigenciaFinal(dataTermino);
    	campanhaService.saveOrUpdate(campanha1);
    	Campanha campanha2 = new Campanha();
    	campanha2.setIdTimeCoracao(3);
    	campanha2.setNomeCampanha("Campanha do Palmeiras");
    	campanha2.setDataVigenciaInicial("07/12/2018"); 
    	campanha2.setDataVigenciaFinal("07/12/2018");
    	campanhaService.saveOrUpdate(campanha2);
    	Campanha campanha3 = new Campanha();
    	campanha3.setIdTimeCoracao(4);
    	campanha3.setNomeCampanha("Campanha do Santos");
    	campanha3.setDataVigenciaInicial("06/12/2017"); 
    	campanha3.setDataVigenciaFinal("06/12/2017");	
    	campanhaService.saveOrUpdate(campanha3);
    	
    	Iterable<Campanha> listaCampanha = CampanhaUtil.trataCampanhasAtivas(campanhaService.findAllCurrent(dataAtual));
    	if(listaCampanha != null){
    		campanhaService.saveElements(listaCampanha);
    	}    	
    	
    	return "Carregamento Padrão Realizado";
    }
    
    @RequestMapping(value = "/salvaCampanha",method = RequestMethod.POST)
    public String save(@RequestBody Campanha campanha) {  
    	campanhaService.saveOrUpdate(campanha);
    	Iterable<Campanha> listaCampanha = CampanhaUtil.trataCampanhasAtivas(campanhaService.findAllCurrent(dataAtual));
    	if(listaCampanha != null){
    		campanhaService.saveElements(listaCampanha);
    	}    	
        return "Campanha salva";
    }
    
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/excluirCampanha/{id}", method = RequestMethod.DELETE)
    public String delete(@PathVariable Integer id) {
    	return campanhaService.delete(id);
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/listaCampanhasVigenciaAlterada", method = RequestMethod.GET)
    public Iterable<Campanha> getCampanhasAlteradas() {
    	return campanhaService.findAllAdjustedDate(dataAtual);
    }
    
    
    @RequestMapping(value = "/alteraCampanha",method = RequestMethod.POST)
    public String update(@RequestBody Campanha campanha) {    	
    	campanhaService.saveOrUpdate(campanha);
    	Iterable<Campanha> listaCampanha = CampanhaUtil.trataCampanhasAtivas(campanhaService.findAllCurrent(dataAtual));
    	if(listaCampanha != null){
    		campanhaService.saveElements(listaCampanha);
    	}    	
        return "Campanha alterada";
    }
    
    
    
}
