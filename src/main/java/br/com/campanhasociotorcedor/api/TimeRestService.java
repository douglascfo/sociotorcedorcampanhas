package br.com.campanhasociotorcedor.api;


import lombok.AllArgsConstructor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.campanhasociotorcedor.domain.orm.TimeCoracao;
import br.com.campanhasociotorcedor.service.TimeService;

//API de serviço do time de coração,usada para CRUD e outros serviços do time de coração
@RestController
@AllArgsConstructor
@RequestMapping("timeCoracao")
public class TimeRestService {

	@Autowired
    private TimeService timeService;
		
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public TimeCoracao getById(@PathVariable Integer id) {
    	return timeService.getById(id);
    }
    
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/listatimes", method = RequestMethod.GET)
    public Iterable<TimeCoracao> getAllTimes() {
    	return timeService.findAll();
    }
              
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value="/salvarTime", method = RequestMethod.POST)
    public TimeCoracao save(@RequestBody TimeCoracao timeCoracao) {
        return timeService.saveOrUpdate(timeCoracao);
    }
    
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping(value="/alterarTime", method = RequestMethod.POST)//Verificar
    public TimeCoracao update(@RequestBody TimeCoracao timeCoracao) {
        return timeService.saveOrUpdate(timeCoracao);
    }
     
    
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/carregamentoTimesPadrao", method = RequestMethod.GET)
    public String carregaCampanhas() {  
    	TimeCoracao timeCoracao = new TimeCoracao();
    	timeCoracao.setNomeTime("Corinthians");
    	timeService.saveOrUpdate(timeCoracao);
    	
    	TimeCoracao timeCoracao1 = new TimeCoracao();
    	timeCoracao1.setNomeTime("São paulo");
    	timeService.saveOrUpdate(timeCoracao1);
    	
    	TimeCoracao timeCoracao2 = new TimeCoracao();
    	timeCoracao2.setNomeTime("Santos");
    	timeService.saveOrUpdate(timeCoracao2);
    	
       	TimeCoracao timeCoracao3 = new TimeCoracao();
    	timeCoracao3.setNomeTime("Palmeiras");
    	timeService.saveOrUpdate(timeCoracao3);
    	
    	return "Carregamento Padrão Realizado";
    }
    
    
    
}
