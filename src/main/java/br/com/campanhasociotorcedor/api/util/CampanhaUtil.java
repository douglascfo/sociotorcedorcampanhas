package br.com.campanhasociotorcedor.api.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import br.com.campanhasociotorcedor.domain.orm.Campanha;

//Classe Util para campanha com funcionalidades especificas a mesma 
public class CampanhaUtil {

	public static Iterable<Campanha> trataCampanhasAtivas(Iterable<Campanha> listaCampanhas){	    
    	Campanha campanha = null;
    	Campanha campanhaAnterior = null;
    	List<Campanha> campanhasAjustadas = new ArrayList<Campanha>();
    	Iterator<Campanha> iterator = listaCampanhas.iterator();
    	while(iterator.hasNext()){
    		campanha = iterator.next();
    		if(campanhaAnterior != null && campanhaAnterior.getDataVigenciaFinal().equals(campanha.getDataVigenciaFinal())){
    			campanha.setDataVigenciaFinal(trataDataFinal(campanha.getDataVigenciaFinal()));
    			campanha.setVigenciaAlterada(true);
    			campanhasAjustadas.add(campanha);
    		}
    		campanhaAnterior = campanha;    	
    	}    	
		return campanhasAjustadas;
	}
		
	private static String trataDataFinal(String data){
		String [] dataAjuste = data.split("/");
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Calendar calendar = new GregorianCalendar(new Integer(dataAjuste[2]),(new Integer(dataAjuste[1])-1),new Integer(dataAjuste[0]));
		calendar.add(Calendar.DAY_OF_YEAR, 1);
		return sdf.format(calendar.getTime());
	}
}
