package br.com.campanhasociotorcedor.domain.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.campanhasociotorcedor.domain.orm.Campanha;

//Interface usada para personalizar queries da camada DAO da entidade Campanha
@Repository
public interface CampanhaRepository extends CrudRepository<Campanha, Integer> {
		
	@Query("from Campanha c where c.dataVigenciaInicial<=:dataAtual and c.dataVigenciaFinal>=:dataAtual order by c.dataVigenciaFinal")
	public Iterable<Campanha> findAllCurrent(@Param("dataAtual") String dataAtual);
	
	@Query("from Campanha c where c.dataVigenciaInicial<=:dataAtual and c.dataVigenciaFinal>=:dataAtual and c.vigenciaAlterada=true order by c.dataVigenciaFinal ")
	public Iterable<Campanha> findAllAdjustedDate(@Param("dataAtual") String dataAtual);
	
}