package br.com.campanhasociotorcedor.domain.repository;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.campanhasociotorcedor.domain.orm.Cliente;

//Interface usada para personalizar queries da camada DAO da entidade Cliente
@Repository
public interface ClienteRepository extends CrudRepository<Cliente, Integer> {
	
	@Query("from Cliente c where c.email<=:email ")
	public Cliente findByEmail(@Param("email") String email);
}
