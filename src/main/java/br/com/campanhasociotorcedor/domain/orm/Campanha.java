package br.com.campanhasociotorcedor.domain.orm;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

//Entidade Campanha
@Entity
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Campanha {	

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Integer idCampanha;	
	private Integer idTimeCoracao;		
	private String nomeCampanha;	
	private String dataVigenciaInicial;
	private String dataVigenciaFinal;
	private Boolean vigenciaAlterada;
	
	@ManyToOne
	private Cliente cliente;
		
	public Integer getIdCampanha() {
		return idCampanha;
	}
	public void setIdCampanha(Integer idCampanha) {
		this.idCampanha = idCampanha;
	}
	public String getNomeCampanha() {
		return nomeCampanha;
	}
	public void setNomeCampanha(String nomeCampanha) {
		this.nomeCampanha = nomeCampanha;
	}
	public Integer getIdTimeCoracao() {
		return idTimeCoracao;
	}
	public void setIdTimeCoracao(Integer idTimeCoracao) {
		this.idTimeCoracao = idTimeCoracao;
	}
	
	public String getDataVigenciaInicial() {
		return dataVigenciaInicial;
	}
	public void setDataVigenciaInicial(String dataVigenciaInicial) {
		this.dataVigenciaInicial = dataVigenciaInicial;
	}
	public String getDataVigenciaFinal() {
		return dataVigenciaFinal;
	}
	public void setDataVigenciaFinal(String dataVigenciaFinal) {
		this.dataVigenciaFinal = dataVigenciaFinal;
	}
	public Boolean getVigenciaAlterada() {
		return vigenciaAlterada;
	}
	public void setVigenciaAlterada(Boolean vigenciaAlterada) {
		this.vigenciaAlterada = vigenciaAlterada;
	}
	
}
