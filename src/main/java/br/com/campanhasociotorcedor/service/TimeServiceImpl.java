package br.com.campanhasociotorcedor.service;


import static java.util.Objects.isNull;
import lombok.AllArgsConstructor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.campanhasociotorcedor.domain.orm.TimeCoracao;
import br.com.campanhasociotorcedor.domain.repository.TimeRepository;

//Classe responsavel pela ligação com a camada de persistencia da Time de Coracao
@Service
@AllArgsConstructor
public class TimeServiceImpl implements TimeService {

	@Autowired
    private TimeRepository timeRepository;
	
    @Override
    public Iterable<TimeCoracao> findAll() {
        return timeRepository.findAll();
    }
	
    @Override
    public TimeCoracao getById(Integer id) {
    	TimeCoracao timeCoracao = timeRepository.findOne(id);
        if(isNull(timeCoracao)) {
            throw new RuntimeException("Time Coração não encontrado!");
        }
        return timeCoracao;
    }

    @Override
    public TimeCoracao saveOrUpdate(TimeCoracao timeCoracao) {
        return timeRepository.save(timeCoracao);
    }
    
    
    @Override
    public String delete(Integer id) {
    	try{
    		timeRepository.delete(id);
    	}catch(Exception ex){
    		return "Problema Deleção timeRepository";	
    	}    	
        return "Cliente Removida";
    }
    
    
}
