package br.com.campanhasociotorcedor.service;

import static java.util.Objects.isNull;
import lombok.AllArgsConstructor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.campanhasociotorcedor.domain.orm.Campanha;
import br.com.campanhasociotorcedor.domain.repository.CampanhaRepository;

//Classe responsavel pela ligação com a camada de persistencia da Campanha
@Service
@AllArgsConstructor
public class CampanhaServiceImpl implements CampanhaService {

	@Autowired
    private CampanhaRepository campanhaRepository;
	
    @Override
    public Iterable<Campanha> findAll() {
        return campanhaRepository.findAll();
    }
    
    @Override
    public Iterable<Campanha> findAllCurrent(String data) {
    	return campanhaRepository.findAllCurrent(data);
    }

        
    @Override
    public Campanha getById(Integer id) {
    Campanha campanha = campanhaRepository.findOne(id);
        if(isNull(campanha)) {
            throw new RuntimeException("Campanha não encontrada!");
        }
        return campanha;
    }

    @Override
    public Campanha saveOrUpdate(Campanha campanha) {
        return campanhaRepository.save(campanha);
    }
    
    @Override
    public Iterable<Campanha> saveElements(Iterable<Campanha> campanhas) {
        return campanhaRepository.save(campanhas);
    }
    
    
    @Override
    public String delete(Integer id) {
    	try{
    		campanhaRepository.delete(id);
    	}catch(Exception ex){
    		return "Problema Deleção Campanha";	
    	}    	
        return "Campanha Removida";
    }
    
	@Override
	public Iterable<Campanha> findAllAdjustedDate(String data) {
		return campanhaRepository.findAllAdjustedDate(data);
	}
    
}
