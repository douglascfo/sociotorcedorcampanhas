package br.com.campanhasociotorcedor.service;


import br.com.campanhasociotorcedor.domain.orm.Cliente;

//Interface Cliente
public interface ClienteService {

	Cliente getByEmail(String email);
	Cliente getById(Integer id);
	String delete(Integer id);
	Cliente saveOrUpdate(Cliente cliente);
	Iterable<Cliente> findAll();
}
