package br.com.campanhasociotorcedor.service;

import br.com.campanhasociotorcedor.domain.orm.Campanha;

//Interface Campanha
public interface CampanhaService {

	Campanha getById(Integer id);
	String delete(Integer id);
	Campanha saveOrUpdate(Campanha campanha);
	Iterable<Campanha> findAll();
	Iterable<Campanha> findAllCurrent(String data);
	Iterable<Campanha> saveElements(Iterable<Campanha> campanhas);
	Iterable<Campanha> findAllAdjustedDate(String data);
	
}
