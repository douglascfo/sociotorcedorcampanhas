package br.com.campanhasociotorcedor.api;

import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import br.com.campanhasociotorcedor.domain.orm.TimeCoracao;
import br.com.campanhasociotorcedor.service.TimeService;


public class TimeRestServiceTest{

	@Mock
	private TimeService timeService;
	@Mock
	private TimeCoracao timeCoracao;	
	@Mock
	private Iterable<TimeCoracao> listaTimes;
	
	
	@Before
	public void setupMock(){
		 MockitoAnnotations.initMocks(this);
	}	
	
	@Test
	public void testGetById() {
		Mockito.when(timeService.getById(10)).thenReturn(timeCoracao);
		assertNotNull(timeCoracao);
	}

	@Test
	public void testCarregaTimes() {
		Mockito.when(timeService.saveOrUpdate(timeCoracao)).thenReturn(timeCoracao);
		assertNotNull(timeCoracao);	
	}
	
}
