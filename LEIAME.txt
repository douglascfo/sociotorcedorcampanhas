+# LEIAME #
+
+* Version 1.0
+* LEIAME Projeto Campanhas Socio Torcedor
+
+### Configura��es e Documenta��es de uso de algumas funcionalidades ###
+* 
+* 1-Baixar as bibliotecas de dependecias usando o Maven
+* 2-Inicializar a aplica��o rodando o metodo 'main' da classe br.com.campanhasociotorcedor.Application 
+* 3-Acessar a API dos servi�os que se encontram no pacote br.com.campanhasociotorcedor.api
+* 4-Os servi�os referentes a Campanha,Cliente e Time de Cora��o poder�o ser acessados usando as strings dos value dos @RequestMapping
+* 5-Segue explica��o de alguns desses servi�os principais para Campanha:
+* 5.1-caminho:"campanha/listaCampanhasAtivas" uso:utilizado para mostrar todas Campanhas que est�o dentro da data vigente
+* 5.2-caminho:"campanha/carregamentoPadrao" uso:utilizado para carregar algumas Campanhas de exemplo
+* 5.3-caminho:"campanha/listaCampanhasVigenciaAlterada" uso:utilizado para mostrar todas Campanhas que tiveram suas data de vigencia final alteradas conforme regra de n�o duplicidade
+* 5.4-outros caminho referente a CRUD:"campanha/excluirCampanha/{id}","campanha/alteraCampanha","campanha/salvaCampanha" e "campanha/{id}"
+* 6-Segue explica��o de alguns desses servi�os principais para Cliente:
+* 6.1-caminho:"cliente/carregamentoClientePadrao" uso:utilizado para carregar alguns Clientes de exemplo
+* 6.2-caminho:"cliente/vincularClienteCampanha" uso:utilizado para vincular um Cliente com uma ou varias Campanhas
+* 6.3-outros caminho referente a CRUD:"cliente/excluirCliente/{id}","cliente/alterarCliente","cliente/salvarCliente","cliente/listaClientes" e "cliente/{id}"
+* 7-Segue explica��o de alguns desses servi�os principais para Time de Cora��o:
+* 7.1-caminho:"timeCoracao/carregamentoTimesPadrao" uso:utilizado para carregar alguns Times de exemplo
+* 7.2-outros caminho referente a CRUD:"timeCoracao/alterarTime","timeCoracao/salvarTime","timeCoracao/listatimes" e "timeCoracao/{id}"
+* 8-Observa��es: Esse projeto � totalmente autonomo, n�o sendo necessario possuir instalado em sua maquina um servidor de aplica��o e nem precisar de um banco de dados rodando
+* pois os dados s�o trabalhados em mem�ria usando ferramentas ageis para performance.
+* 9-Linguagem,Frameworks,API's usadas: Java,Spring Boot,Spring Data,JPA,Banco H2,Mockito,JUnit e outros.
+* 10-Estrategia utilizada: Foi usada ferramentas atuais e ageis como Spring Boot que disponibiliza uma aplica��o que pode ser rodada em qualquer maquina apenas usando Java,
+* como banco de dados foi usado o H2 pois � agil para rodar grande massa de dados em mem�ria sem a necessidade de instala��o de banco na maquina,
+* foi usado padr�es de projeto visando a agilidade e qualidade do desenvolvimento e facilidade de manuten��o por qualquer outro desenvolvedor,padr�es como:DAO,DI,MVC,Iterator,Template,Factory Method,Singleton e outros.
+
+* Criado por Douglas Oliveira